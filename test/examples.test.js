
const { Builder, By } = require("selenium-webdriver");

describe("calculator tests", () => {
  let driver;

  beforeAll(async () => {
    driver = new Builder().forBrowser("chrome").build();
    await driver.get("http://www.anaesthetist.com/mnm/javascript/calc.htm");
  }, 10000);

  afterAll(async () => {
    await driver.quit();
  }, 10000);

  test("should multiply correctly", async () => {
    await driver.findElement(By.name("clear")).click();
    await driver.findElement(By.name("nine")).click();
    await driver.findElement(By.name("mul")).click();
    await driver.findElement(By.name("three")).click();
    await driver.findElement(By.name("result")).click();

    const value = await driver
      .findElement(By.name("Display"))
      .getAttribute("value");

    expect(value).toEqual("27");
  });

  test("should multiply correctly", async () => {
    await driver.findElement(By.name("clear")).click();
    await driver.findElement(By.name("two")).click();
    await driver.findElement(By.name("mul")).click();
    await driver.findElement(By.name("seven")).click();
    await driver.findElement(By.name("result")).click();

    const value = await driver
      .findElement(By.name("Display"))
      .getAttribute("value");

    expect(value).toEqual("14");
  });

  test("should divide correctly", async () => {
    await driver.findElement(By.name("clear")).click();
    await driver.findElement(By.name("eight")).click();
    await driver.findElement(By.name("div")).click();
    await driver.findElement(By.name("two")).click();
    await driver.findElement(By.name("result")).click();

    const value = await driver
      .findElement(By.name("Display"))
      .getAttribute("value");

    expect(value).toEqual("4");
  });

  test("should make screenshot", async () => {
    const screenshot = await driver.takeScreenshot();
    await require("fs").promises.writeFile("screenshot.png", screenshot, "base64");
  });
});
